<?php
declare(strict_types=1);

namespace Secure\Infrastructure\Persistence\InMemory\User;


use Secure\Domain\User\Model\User;
use Secure\Domain\User\Repository\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    private array $users;

    public function __construct()
    {
        $this->users = [
            'Lucy' => new User('Lucy Lane', 'Lucy', 'passwordLucy'),
            'Alex' => new User('Lex Luthor', 'Alex', 'passwordAlex'),
        ];
    }

    public function getUserByLogin(string $login): ?User
    {
        return $this->users[$login] ?? null;
    }
}
