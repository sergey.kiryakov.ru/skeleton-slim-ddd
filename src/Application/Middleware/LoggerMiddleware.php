<?php
declare(strict_types=1);


namespace Secure\Application\Middleware;


use Libs\Timer\TimerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;

class LoggerMiddleware implements MiddlewareInterface
{
    private LoggerInterface $logger;
    private TimerInterface $timer;

    public function __construct(
        LoggerInterface $logger,
        TimerInterface $timer
    )
    {
        $this->logger = $logger;
        $this->timer = $timer;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->timer->start();
        $response = $handler->handle($request);
        $this->logger->debug(
            'Запрос',
            [
                'time-process' => $this->timer->diff(),
                'query-url' => $request->getUri()->getPath(),
                'query-params' => $request->getUri()->getQuery(),
                'response' => $response->getBody()->__toString()
            ]
        );

        return $handler->handle($request);
    }
}
