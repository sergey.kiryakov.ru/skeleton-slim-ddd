<?php
declare(strict_types=1);

namespace Secure\Application\Middleware\Validate\User;


use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Secure\Domain\User\Assembler\In\UserLoginDTOAssembler;
use Secure\Domain\User\Validator\UserLoginValidator;
use Slim\Psr7\Response;

class LoginValidator implements MiddlewareInterface
{
    private UserLoginDTOAssembler $assembler;
    private UserLoginValidator $validator;

    public function __construct(
        UserLoginDTOAssembler $assembler,
        UserLoginValidator $validator
    )
    {
        $this->assembler = $assembler;
        $this->validator = $validator;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $dto = $this->assembler->toDTO($request->getQueryParams());
        if (!$this->validator->validate($dto)) {
            $response = new Response();
            $errors = [
                'errors' => $this->validator->getErrors(),
            ];
            $response->getBody()->write(json_encode($errors));
            $response = $response->withStatus(StatusCodeInterface::STATUS_BAD_REQUEST)
                ->withHeader('Content-Type', 'application/json');
        } else {
            $response = $handler->handle($request);
        }

        return $response;
    }
}
