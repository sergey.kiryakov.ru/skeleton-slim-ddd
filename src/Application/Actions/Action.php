<?php

namespace Secure\Application\Actions;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Slim\Psr7\Request;

abstract class Action
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;
    /**
     * @var Request
     */
    protected Request $request;
    /**
     * @var ResponseInterface
     */
    protected ResponseInterface $response;
    /**
     * @var array
     */
    protected array $args;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    abstract protected function action(): ResponseInterface;

    public function __invoke(RequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;
        return $this->action();
    }

    protected function responseJson($data = null, $status = 200): ResponseInterface
    {
        $json = json_encode($data, JSON_PRETTY_PRINT);
        $this->response->getBody()->write($json);
        return $this->response
            ->withStatus($status)
            ->withHeader('Content-Type', 'application/json')
        ;
    }
}
