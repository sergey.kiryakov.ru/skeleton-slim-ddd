<?php
declare(strict_types=1);


namespace Secure\Application\Actions\User;


use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Secure\Application\Actions\Action;
use Secure\Domain\User\Assembler\In\UserLoginDTOAssembler;
use Secure\Domain\User\Assembler\Out\UserLoginAssembler;
use Secure\Domain\User\Service\UserLoginService;

final class UserLogin extends Action
{
    private UserLoginDTOAssembler $assemblerIn;
    private UserLoginAssembler $assemblerResponse;
    private UserLoginService $service;

    public function __construct(
        LoggerInterface $logger,
        UserLoginDTOAssembler $assemblerIn,
        UserLoginAssembler $assemblerResponse,
        UserLoginService $service
    )
    {

        parent::__construct($logger);
        $this->assemblerIn = $assemblerIn;
        $this->assemblerResponse = $assemblerResponse;
        $this->service = $service;
    }

    protected function action(): ResponseInterface
    {
        return $this->responseJson(
            $this->assemblerResponse->toResponse(
                $this->service->auth(
                    $this->assemblerIn->toDTO($this->request->getQueryParams())
                )
            )
        );
    }
}
