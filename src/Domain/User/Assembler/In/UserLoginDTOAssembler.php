<?php
declare(strict_types=1);


namespace Secure\Domain\User\Assembler\In;


use Secure\Domain\User\DTO\UserLoginDTO;

class UserLoginDTOAssembler
{
    private const FILTERED_BY_LOGIN = 'login';
    private const FILTERED_BY_PASSWORD = 'password';


    public function toDTO(array $params): UserLoginDTO
    {
        return new UserLoginDTO(
            $params[self::FILTERED_BY_LOGIN] ?? null,
            $params[self::FILTERED_BY_PASSWORD] ?? null
        );
    }
}
