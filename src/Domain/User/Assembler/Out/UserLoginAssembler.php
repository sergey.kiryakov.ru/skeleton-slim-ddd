<?php
declare(strict_types=1);


namespace Secure\Domain\User\Assembler\Out;


use Secure\Domain\User\Model\User;

class UserLoginAssembler
{
    public function toResponse(?User $user): array
    {
        if (!$user) {
            $result = [
                'status' => 'failed',
                'data' => null
            ];
        }

        if (!($result ?? null)) {
            $result = [
                'status' => 'success',
                'data' => [
                    'login' => $user->getLogin(),
                    'name' => $user->getName()
                ]
            ];
        }

        return $result ?? null;
    }
}
