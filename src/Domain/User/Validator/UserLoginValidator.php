<?php
declare(strict_types=1);

namespace Secure\Domain\User\Validator;


use Secure\Domain\User\DTO\UserLoginDTO;

class UserLoginValidator
{
    public function validate(UserLoginDTO $DTO)
    {
        $result = true;
        $login = $DTO->getLogin();
        $password = $DTO->getPassword();

        if (!$login || !$password || $login === '' || $password === '') {
            $result = false;
        }

        return $result;
    }

    public function getErrors()
    {
        return [
            'description' => 'Не заполнены обязательные поля',
        ];
    }
}
