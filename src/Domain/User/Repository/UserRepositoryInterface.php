<?php
declare(strict_types=1);

namespace Secure\Domain\User\Repository;


use Secure\Domain\User\Model\User;

interface UserRepositoryInterface
{
    public function getUserByLogin(string $login): ?User;
}
