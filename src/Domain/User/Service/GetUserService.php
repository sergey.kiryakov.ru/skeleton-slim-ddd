<?php
declare(strict_types=1);

namespace Secure\Domain\User\Service;


use Secure\Domain\User\Model\User;
use Secure\Domain\User\Repository\UserRepositoryInterface;

class GetUserService
{
    private UserRepositoryInterface $repository;

    public function __construct(
        UserRepositoryInterface $repository
    )
    {
        $this->repository = $repository;
    }

    public function getUserByLogin(string $login): ?User
    {
        return $this->repository->getUserByLogin($login);
    }
}
