<?php
declare(strict_types=1);


namespace Secure\Domain\User\Service;


use Secure\Domain\User\DTO\UserLoginDTO;

class UserLoginService
{
    private GetUserService $getUserService;

    public function __construct(
        GetUserService $getUserService
    )
    {
        $this->getUserService = $getUserService;
    }

    public function auth(UserLoginDTO $DTO)
    {

        $user = $this->getUserService->getUserByLogin($DTO->getLogin());
        if ($user && ($user->getPassword() === $DTO->getPassword())) {
            $result = $user;
        }


        return $result ?? null;
    }
}
