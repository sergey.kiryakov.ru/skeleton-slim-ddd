<?php
declare(strict_types=1);


namespace Secure\Domain\User\Model;


class User
{
    private string $name;
    private string $login;
    private string $password;

    public function __construct(
        string $name,
        string $login,
        string $password
    )
    {
        $this->name = $name;
        $this->login = $login;
        $this->password = $password;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
