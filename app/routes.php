<?php
declare(strict_types=1);

use Secure\Application\Actions\User\UserLogin;
use Secure\Application\Middleware\Validate\User\LoginValidator;
use Slim\App;

return static function (App $app) {
    // http://localhost:8080/?login=Lucy&password=passwordLucy
    $app->get('/', UserLogin::class)->add(LoginValidator::class);
};
