<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return static function (ContainerBuilder $containerBuilder) {
    // Глобальные настройки
    $settings = [
        'settings' => [
            'displayErrorDetails' => true,
            'logger' => [
                'name' => 'app',
                'path' => __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'php-di' => [
                'enableCompilation' => false,
                'compilationDir' => __DIR__ . '/../var/php-di/cache'
            ],
            'settings' => [
                'config' => [

                ],
            ],
        ]
    ];
    $containerBuilder->addDefinitions($settings);

    if ($settings['settings']['php-di']['enableCompilation']) {
        $containerBuilder->enableCompilation($settings['settings']['php-di']['compilationDir']);
    }
};
