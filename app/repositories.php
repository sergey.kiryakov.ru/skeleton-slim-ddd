<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Secure\Domain\User\Repository\UserRepositoryInterface;
use Secure\Infrastructure\Persistence\InMemory\User\UserRepository;

return static function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions(
        [
            UserRepositoryInterface::class => DI\get(UserRepository::class)
        ]
    );
};
