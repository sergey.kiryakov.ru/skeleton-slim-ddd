<?php
declare(strict_types=1);

use Config\GlobalConfig;
use DI\ContainerBuilder;
use Libs\Timer\Timer;
use Libs\Timer\TimerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return static function (ContainerBuilder $containerBuilder) {
    // Определение всех библиотечных зависимостей
    $containerBuilder->addDefinitions(
        [
            LoggerInterface::class => static function (ContainerInterface $c) {
                $settings = $c->get('settings');

                $loggerSettings = $settings['logger'];
                $logger = new Logger($loggerSettings['name']);

                $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
                $logger->pushHandler($handler);

                return $logger;
            },

            GlobalConfig::class => static function (ContainerInterface $c) {
                $settings = $c->get('settings');
                $config = $settings['config'];
                return GlobalConfig::createConfig($config);
            },

            TimerInterface::class => static function () {
                return new Timer();
            }
        ]
    );
};
