<?php
declare(strict_types=1);

use Secure\Application\Middleware\LoggerMiddleware;
use Slim\App;

return static function (App $app) {
    $app->add(LoggerMiddleware::class);
};
