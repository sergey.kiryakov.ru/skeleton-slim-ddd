<?php
declare(strict_types=1);

namespace Config;


class GlobalConfig
{
    private array $settingsData;

    private function __construct(array $settingsData)
    {
        $this->settingsData = $settingsData;
    }

    public static function createConfig(array $settingsData): self
    {
        return new self($settingsData);
    }
}
