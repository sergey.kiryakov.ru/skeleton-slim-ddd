<?php
declare(strict_types=1);

use Psr\Log\LoggerInterface;
use Secure\Application\Handler\ErrorHandler;
use Secure\Application\Handler\ShutdownHandler;
use Slim\App;
use Slim\Factory\ServerRequestCreatorFactory;

setlocale(LC_ALL, "ru_RU.UTF-8");
/** @var App $app */
$app = require __DIR__ . '/../app/bootstrap.php';

// Register middleware
$middleware = require __DIR__ . '/../app/middleware.php';
$middleware($app);

// Register routes
$routes = require __DIR__ . '/../app/routes.php';
$routes($app);

/** @var bool $displayErrorDetails */
$displayErrorDetails = $container->get('settings')['displayErrorDetails'];

// Create Request object from globals
$serverRequestCreator = ServerRequestCreatorFactory::create();
$request = $serverRequestCreator->createServerRequestFromGlobals();

// Add Routing Middleware
$app->addRoutingMiddleware();

// Create Error Handler
$responseFactory = $app->getResponseFactory();
$errorHandler = new ErrorHandler($app->getCallableResolver(), $responseFactory, $container->get(LoggerInterface::class));

// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, true, true);

$shutdownHandler = new ShutdownHandler($request, $errorHandler, $displayErrorDetails);
register_shutdown_function($shutdownHandler);

$errorMiddleware->setDefaultErrorHandler($errorHandler);
$app->run();
