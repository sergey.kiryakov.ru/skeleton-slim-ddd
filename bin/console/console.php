<?php

require __DIR__ . '/../../vendor/autoload.php';
/** @var App $app */
$app = require __DIR__ . '/../../app/bootstrap.php';
error_reporting(E_ALL & ~E_NOTICE);

use Slim\App;
use Symfony\Component\Console\Application;

$application = new Application();

$application->addCommands([]);
$application->run();
