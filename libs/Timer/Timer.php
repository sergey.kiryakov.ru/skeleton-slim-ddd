<?php
declare(strict_types=1);

namespace Libs\Timer;


class Timer implements TimerInterface
{
    private float $startTime = 0;

    public function start(): void
    {
        $this->startTime = microtime(true);
    }

    public function diff(): float
    {
        return microtime(true) - $this->startTime;
    }
}
