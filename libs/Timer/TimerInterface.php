<?php
declare(strict_types=1);

namespace Libs\Timer;


interface TimerInterface
{
    public function start(): void;

    public function diff(): float;
}
